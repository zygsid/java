/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

/**
 * Cia pagrindine klase, kurioje yra main metodas
 * @author User
 */
public class Main {

    /**
     * Pradinis metodas, nuo kurio pradedamas programos darbas
     * @param args the command line arguments
     */
    private static String kintamasis; //pabaigoje yra getteris ir setteris
    
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Hello world!");
        /*
        Bendri nutarimai(konvencijos):
        
        1. Kintamieji sukuriami klases virsuje
        
        2. Anotacijos prasideda @ zenkleliu (@Override)
        
        3. Javadoc dokumentacija rasoma:
        /*
        *
        * ir uzbaigiamas su *./ (tasko neturi buti)
        
        4. Kompiliuojamos klases pavadinimas turi sutapti su failo pavadinimu (Failas.java), nes ji yra case-sensitive
        
        5. Prieinamumo lygiai yra 4:
            1) Public
            2) Private
            3) default (dazniausiai buna nenurodytas (default class Klase yra tas pats, kas class Klase)
            4) protected (dukterinem klases (hierarchinem arba paveldejimo) yra leidziama pasiekti protected tipo daiktus
        Jeigu norima extendint klase (padaryti paveldimuma), reikia kitame pakete parasyti prie klases pavadinimo extends ir klases pavadinima (class MainNauja extends Main)
        Ir dar reikia importuoti ta klase, kuria extendina (import lt.vcs.Main)
        
        6. Neprieinamumo modifikatoriai:
            1) Final (pvz.: private static final String kintamasis = "Reiksme";) - jo niekada daugiau negalima bus pakeisti
            2) Abstract - metodas arba klase tampa abstraktus (pvz.: public abstract void metodas(){})
            3) Strictfp - tarkim jeigu kintamieji yra ir double, ir float tipu, tai sis modifikatorius suvienodina skaicius po kablelio
        
        7. Kintamieji (objektiniai (sukurti klases tipo objektai) arba primityvus (int, double, float ir t.t.) ):
            1) Klases kintamieji (statiniai kintamieji)
            2) Esybes (objekto) kintamieji (nestatiniai)
            3) Lokalus kintamieji (pvz.: String kintamasis2 = "Reiksme";)
        
        8. Obejektinio tipo kintamuosius (iskaitant ir String) turime lyginti su equals metodu (pvz.: kintamasis.equals(kintamasis2); ir grazina true jei vienodi)
        
        9. For ciklas, jei nori eiti per masyva ar kolekcija:
            String[] masyvas = {"", "", ""};
            for(String elementas : masyvas)
            {}
        
        1
        */
        getKintamasis().equals("Kazkoks textas");
        boolean contains = Main.getKintamasis().contains("a");
        
        
        
    }

    /**
     * @return the kintamasis
     */
    public static String getKintamasis() {
        return kintamasis;
    }

    /**
     * @param aKintamasis the kintamasis to set
     */
    public static void setKintamasis(String aKintamasis) {
        kintamasis = aKintamasis;
    }
    
}
