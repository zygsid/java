/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import static lt.vcs.VcsUtils.*;

/**
 *
 * @author User
 */
public class Main {

        public static final String SELECT_LAST_ID_SQL = "select MAX(id) from %s;";
    
        static String url = "jdbc:mysql://localhost:3306/";
        static String dbName = "vcs_17_04";
        static String userName = "root";
        static String password = "";
    
    public static void main(String[] args) {

        Connection conn = null;
        
        try {
            conn = DriverManager.getConnection(url + dbName, userName, password);
            Statement s = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = s.executeQuery("select * from car;");
            out("--------------- car lenteles turinys (pries insert) --------------------");
            outCarResultSet(rs);
            out("--------------- car lenteles turinys (po insert) --------------------");
//            for(int i=1; rs.next(); i++)
//            {
//                rs.updateString("make", rs.getString("make") + " (kazka kompensuoja) " + i);
//                rs.updateRow();
//            } uzkomentavom, kad nepildytu pastoviai DB data
            rs.moveToInsertRow();
            rs.updateInt("id", getLastId("car")+1);
            rs.updateString("make", "ZAPUKAS");
            rs.updateString("name", "imi ir vaziuoji");
            rs.insertRow();
            outCarResultSet(rs);
            out("--------------- car lenteles turinys (pries delete) --------------------");
            outCarResultSet(rs);
            out("--------------- car lenteles turinys (po delete) --------------------");
            rs.afterLast();
            rs.previous();
            rs.deleteRow();
            outCarResultSet(rs);
            
        } catch (Exception e) {
            out("Ivyko klaida: " + e.getMessage());
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            }
        }
    }
    
    private static void outCarResultSet(ResultSet rs) throws Exception
    {
        rs.beforeFirst();
        while(rs.next())
            {
                out("id ='" + rs.getInt(1) + "' make: '" + rs.getString(2) + "' name: '" + rs.getString(3) + "'");
            }
        rs.beforeFirst();
    }

    private static int getLastId(String table)
        {
            Integer result = null;
            Connection conn = null;
            try{
            conn = DriverManager.getConnection(url + dbName, userName, password);
            Statement s = conn.createStatement();
            //ResultSet rs = s.executeQuery("select id from " + table + " order by id desc limit 1;");
            ResultSet rs = s.executeQuery(String.format(SELECT_LAST_ID_SQL,table));
            if(rs.next())
            {
                result = rs.getInt(1);
            }
            }
            catch(Exception e)
            {
                out(e.getMessage());
                throw new RuntimeException();
            }
            finally
            {
                if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            }
            }
            return result;
        }
}
