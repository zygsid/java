/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author User
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    private static void out(String txt)
    {
        System.out.println(txt);
    }
    
    private static Scanner newScan()
    {
        return new Scanner(System.in);
    }
    
    private static int inInt()
    {
        return newScan().nextInt();
        
    }
    
    private static int sumuok(int... skaiciai) //gali buti bet kiek tokio tipo parametru (nusako, kad parametras neprivalomas, taciau galiu paduoti daug kintamuju). Vieta parametruose turi buti paciame gale!
    {
        int result = 0;
        if(skaiciai != null)
        {
            
        }
        return result;
    }
    
    public static void main(String[] args) 
    {
        String tekstas = "Iveskite skaiciu:";
        out(tekstas);
        int kintamasis = inInt();
        out("" + kintamasis);
        String stringas = "Ivestas sk: %d";
        out(String.format(stringas, kintamasis));
        //CTRL + SPACE ismeta pasiulymu (suggestions) lentele, kuri parodys visas tinkamas kodo tolimesnes rasybos opcijas
        //Formatavimas: %d - decimal, %s - stringas, %i - intas ir t.t.
        
        String[] mass = new String[5];
        String[] mass2 = {"tekstas", "tekstas2"};
        out("" + mass.length); //grazina dydi masyvo
        out("" + mass2[0]); //grazina masyvo turini 0 indexe
        
        for (int i = 0; i < mass2.length; i++) {
            out("" + mass2[i]);        
        }
        
        int[] mass3 = {1,2,3,4,5,6}; //sumuok funkcijos pavyzdys
        sumuok();
        sumuok(7,2,3,3);
        sumuok(mass3);
    }
    
}
