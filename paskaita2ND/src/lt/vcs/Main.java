package lt.vcs;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static void uzduotys(String uzd)
    {
        Scanner scan = new Scanner(System.in);
        if("1a".equals(uzd))
        {
        int[] skaiciai = new int[5];
        int suma = 0;
        for (int i = 0; i < 5; i++) {
            System.out.println("Iveskite " + (i+1) + " skaiciu:");
            int input = scan.nextInt();
            suma += input;
            skaiciai[i] = input;
        }
        System.out.println("Suma: " + suma + "\nSkaiciai:");
        for (int j = 0; j < 5; j++) {
        System.out.println(skaiciai[j]);
        }
        }
        else if("1b".equals(uzd))
        {
            String[] zodziai = new String[5];
        for(int i=0;i<5;i++)
        {
            System.out.println((i+1) + "-as zodis:");
            zodziai[i] = scan.next();
        }
        for(int j=0; j<zodziai.length;j++)
        {
            System.out.print(zodziai[j] + " ");
        }           
        }
        else if("2a".equals(uzd))
        {
        double mase, ugis;
        System.out.println("Iveskite ugi metrais: ");
        ugis = scan.nextDouble();
        System.out.println("Iveskite svori kilogramais: ");
        mase = scan.nextDouble();
        double kmi = mase/(ugis*ugis);
        System.out.println("Jusu KMI: " + kmi);
        }
        else if("2b".equals(uzd))
        {
        System.out.println("Iveskite kiek skaiciu ketinate ivedineti:");
        int kiek = scan.nextInt();
        int[] skaiciaiVirs100 = new int[kiek];
        int skaiciuVirs100Kiekis = 0;
        for (int i = 0; i < kiek; i++) {
            int skaicius;
            System.out.println("Iveskite skaiciu: ");
            skaicius = scan.nextInt();
            if(skaicius > 100)
            {
                skaiciaiVirs100[skaiciuVirs100Kiekis] = skaicius;
                skaiciuVirs100Kiekis++;
            }
        }
        for(int j=0;j<skaiciuVirs100Kiekis;j++)
        {
            System.out.println(skaiciaiVirs100[j]);
        }
        }
    }
    
    public static void main(String[] args) {
        System.out.println("Iveskite uzduoties nr (1a/b ar 2a/b), kuria norite vykdyti:");
        String uzd;
        Scanner scan = new Scanner(System.in);
        uzd = scan.next();
        uzduotys(uzd);
    }
}
