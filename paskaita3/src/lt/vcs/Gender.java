/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.util.logging.Logger;

/**
 *
 * @author User
 */
public enum Gender {
    Male("Male", "Vyras", 1), //tas pats kas rasyti Male(),
    Female("Female", "Moteris", 0),
    Other("Other", "Kita", 2);
    
    private int id;
    
    //konstruktoriai
    private String enLabel;
    private String ltLabel;
    private Gender(String enLabel,String ltLabel, int id)
    {
        this.enLabel = enLabel;
        this.ltLabel = ltLabel;
        this.id = id;
    }
    public static Gender getById(int id)
    {
        for(Gender gen : Gender.values())
        {
            if(id == gen.getId())
            {
                return gen;
            }
        }
        return null;
    }

    public int getId() {
        return id;
    }

    public String getEnLabel() {
        return enLabel;
    }

    public String getLtLabel() {
        return ltLabel;
    }
}
