/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;
import java.text.SimpleDateFormat;
import java.util.Date;
import static lt.vcs.VcsUtils.*; //ZVAIGZDUTE REISKIA VISKA (IMPORT ALL)
/**
 *
 * @author User
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    
    private static String naujasIfas(boolean salyga)
    {
        return salyga ? "tikrai abc" : "ne abc";
    }
        
    public static void main(String[] args) {
//        // TODO code application logic here
//        out("Iveskite triju raidziu zodi");
//        String abc = inWord();
//        if(abc !=  null && "abc".equals(abc))
//        {
//            out("tikrai abc");
//        }
//        else out("ne abc");
//        
//        //NAUJA IF SALYGA
//        String result = naujasIfas(abc !=  null && "abc".equals(abc));
//        //arba galima result = (abc !=  null && "abc".equals(abc)) ? "tikrai abc" : "ne abc";
//        out(result);
//        
//        //DATOS SPAUSDINIMAS
//        
//        out(new Date());
//        SimpleDateFormat sdf = new SimpleDateFormat("'Data: 'yyyy-MM-dd 'Laikas:' HH:mm:ss"); //formatavimas
//        //Viengubos kabutes '' iterpia custom texta i formata
//        out(sdf.format(new Date())); //spausdinimas
//        //String laikas = timeNow();
//        //out(laikas);
//        
//        //Random
//        out(random(1,10));
//        
        
        //Player klase
        out("Norint zaisti su vienu kauliuko losimu iveskite 0, o su 5 losimais - 1:");
        int userChoseGame = inInt();
        if(userChoseGame == 0)
        {
        out("Sveiki, jus zaidziate kauliuku zaidima, skirta dviem zaidejams!");
        out("Iveskite pirmo zaidejo varda:");
        String player1Name = inWord();
        out("Iveskite pirmo zaidejo lyti(Mot - 0, Vyr - 1, kita - 2):");
        int sex1 = inInt();
        out("Iveskite antro zaidejo varda:");
        String player2Name = inWord();
        out("Iveskite antro zaidejo lyti(Mot - 0, Vyr - 1, kita - 2):");
        int sex2 = inInt();
        //kuriam zaideju objektus
        Player player1 = new Player(player1Name, Gender.getById(sex1));
        Player player2 = new Player(player2Name, Gender.getById(sex2));
        out("Buvo sukurti du nauji zaidejai ir jie ridena kauliukus!");
        out(player1.getName() + " isrideno " + player1.getDice());
        out(player2.getName() + " isrideno " + player2.getDice());
        if(player1.getDice() > player2.getDice())
            out("Laimejo " + player1.getName() +  "!");
        else if (player1.getDice() < player2.getDice())
        {
            out("Laimejo " + player2.getName() + "!");
        }
        else out("Lygiosios!");
        }
        else if(userChoseGame == 1)
        {
        out("Sveiki, jus zaidziate kauliuku zaidima, skirta dviem zaidejams!");
        out("Iveskite pirmo zaidejo varda:");
        String player1Name = inWord();
        out("Iveskite pirmo zaidejo lyti(Mot - 0, Vyr - 1, kita - 2):");
        int sex1 = inInt();
        out("Iveskite antro zaidejo varda:");
        String player2Name = inWord();
        out("Iveskite antro zaidejo lyti(Mot - 0, Vyr - 1, kita - 2):");
        int sex2 = inInt();
        
        Player player1 = new Player(player1Name, Gender.getById(sex1));
        Player player2 = new Player(player2Name, Gender.getById(sex2));
        out("Buvo sukurti du nauji zaidejai ir jie ridena kauliukus po 5 kartus!");
        out(player1.getName() + " isrideno " + player1.getDiceHandScore());
        out(player2.getName() + " isrideno " + player2.getDiceHandScore());
        if(player1.getDiceHandScore()> player2.getDiceHandScore())
            out("Laimejo " + player1.getName() +  "!");
        else if (player1.getDiceHandScore()< player2.getDiceHandScore())
        {
            out("Laimejo " + player2.getName() + "!");
        }
        else out("Lygiosios!");
        }
//        //Enumai
//        out(Gender.Female.getLtLabel());

    }
    
}
