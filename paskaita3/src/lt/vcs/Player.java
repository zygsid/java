/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;
import static lt.vcs.VcsUtils.*;
/**
 *
 * @author User
 */
public class Player {
    private String name;
    private int dice;
    private Gender gender;
    private int[] diceHand;
    public Player(String name, Gender gender)
    {
        this.name = name == null ? name : name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
        this.dice = random(1,6);
        this.gender = gender;
        this.diceHand = new int[5];
        for(int i=0;i<5;i++)
        {
        this.diceHand[i] = random(1, 6);
        }
    }
    public String getName()
    {
        return this.name;
    }

    public int getDice() {
        return dice;
    }

    public Gender getGender() {
        return gender;
    }

    public int[] getDiceHand() {
        return diceHand;
    }
    public int getDiceHandScore()
    {
        int score = 0;
        for(int i=0;i<5;i++)
        {
            score += this.diceHand[i];
        }
        return score;
    }
    
}
