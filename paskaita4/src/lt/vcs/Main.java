/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;
import java.util.ArrayList;
import java.util.List;
import static lt.vcs.VcsUtils.*;
/**
 *
 * @author User
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Player p1 = new Player(inWord("Iveskite varda:"),Gender.getById(inInt("Iveskite lyti (M - 0, V - 1, KT - 2)")));
        Person per1 = p1;
        Object obj = p1;


//      /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
        //VIRSAUS KODAS YRA LYGUS APACIOS VISOMS EILUTEMS :)
//      \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\  
//        String name = inWord("Iveskite varda:");
//        int lytis = inInt("Iveskite lyti:");
//        Gender gen = Gender.getById(lytis);
//        out(new Player(name, gen));

        out(p1);
        out(per1);
        out(obj);
        
        User<Person> u1 = new User(per1); //si deimantine parametru sintakse dabar leis pasiekti String metodus
        out(u1.getPerson()); //u1.getPerson().* galima del <String> pasiekti string metodus
        User<Player> u2 = new User(p1);
        out(u2.getPerson()); //galima dabar jau kviesti Player metodus
        
        List lst = new ArrayList();
        
        
    }
    
}
