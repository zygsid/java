/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

/**
 *
 * @author User
 */
public class Person {
    
    private String name;
    private String surname;
    protected Gender gender;
    private int age;
    public Person(String name)
    {
        this.name = name == null ? name : name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
    }
    public Person(String name, Gender gender)
    {
        this(name);
        this.gender = gender;
    }
    public Person(String name, Gender gender, int age, String surname)
    {
        this(name,gender);
        this.age = age;
        this.surname = surname;
    }
    
    @Override
    public String toString()
    {
        return getClass().getSimpleName() + "(name = " + getName() + " gender = " + gender.getEnLabel() + ")";
    }


    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Gender getGender() {
        return gender;
    }

    public int getAge() {
        return age;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
