/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;
import static lt.vcs.VcsUtils.*;
/**
 *
 * @author User
 */
public class Player extends Person {
    private int dice;
    private int[] diceHand;
    
    public Player(String name, Gender gender)
    {
        super(name, gender);
        this.dice = random(1,6);
        this.diceHand = new int[5];
        for(int i=0;i<5;i++)
        {
        this.diceHand[i] = random(1, 6);
        }
    }
    
    public Player(String name)
    {
        super(name);
        this.dice = random(1,6);
        for(int i=0;i<5;i++)
        {
        this.diceHand[i] = random(1, 6);
        }
    }
    public Player(String name, Gender gender, int age, String surname)
    {
        this(name, gender);
        setAge(age);
        setSurname(surname);
        
    }
    
    @Override
    public String toString()
    {
        return super.toString().replaceFirst("\\)", " dice = " + dice +")"); //backslashai escapina simboli ), nes kitaip meta exceptionu daug
    }
    
    public int getDice() {
        return dice;
    }

    public int[] getDiceHand() {
        return diceHand;
    }
    public int getDiceHandScore()
    {
        int score = 0;
        for(int i=0;i<5;i++)
        {
            score += this.diceHand[i];
        }
        return score;
    }
    
}
