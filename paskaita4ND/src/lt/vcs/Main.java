/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Namai
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    private static void out(String text)
    {
        System.out.println(text);
    }
    
    private static void palindromasCheck(String text)
    {
        String zodis = text.replaceAll("\\s+", "");
        zodis = zodis.toUpperCase();
        String reverse = new StringBuffer(zodis).reverse().toString();
        if(zodis.equals(reverse))
        {
            out("Sakinys/zodis yra palindromas");
        }
        else out("Sakinys/zodis nera palindromas");
        
    }
    
    public static void main(String[] args) {
        
        
        out("Iveskite sakini(zodi), kuri tikrinsim:");
        Scanner scan = new Scanner(System.in);
        String tekstas = scan.nextLine();
        palindromasCheck(tekstas);
        
        ArrayList listas = new ArrayList();
        listas.add(2);
        listas.add(5);
        listas.add(1);
        listas.add(9);
        listas.add(-999);
        listas.add(9879);
        listas.add(55);
        listas.add(26);
        Collections.sort(listas);
        for(Object item : listas)
        {
            out(item.toString());
        }
        Collections.reverse(listas);
        for(Object item : listas)
        {
            out(item.toString());
        }

        
        

        
        
        
        
        
        
    }
    
}
