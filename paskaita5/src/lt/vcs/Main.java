/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static lt.vcs.VcsUtils.*;
/**
 *
 * @author User
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String line = inLine("Iveskite betkokius zodziu, atskirtus kableliu:");
        line = line.replaceAll(" ", "");
        String[] lineArr = line.split(","); //isskaidys musu ivesta stringa i masyva (po kablelio eis naujas masyvo elementas!)
        //Masyvas bus toks: lineArr[] = {"Zodis" , "kitas zodis po kabalelio"}
        List<String> lineList = createList(lineArr);
        Set<String> lineSet = createSet(lineArr);
        Iterator<String> iter = lineList.iterator();
        //iteratorius
        while(iter.hasNext())
        {
            String iterItem = iter.next();
            iter.remove(); //pasalintu objekta auksciau (paskutini nextinta objekta)
        }
        lineList.size();//pasako kiek nariu yra musu kolekcijoje
        //salinimas elemento is listo foreach ciklo pagalba
        for(String str : lineList)
        {
            lineList.remove("dfg"); //pasalintu sita is listo
        }
        out("-------------------lineList------------------------");
        collectionOut(lineList);
        out("-------------------lineSet------------------------");
        collectionOut(lineSet);
        out("-------------------lineListSorted------------------------");
        Collections.sort(lineList); //atbuline tvarka Collections.reverse(lineList); bet cia turi eiti po sortinimo, nes reverse nerusiuoja, bet sukeicia elementu eiliskuma atbuline tvarka
        collectionOut(lineList);
        //setas negali buti sortinamas!
        
        Map<String, Integer> mapas = new HashMap(); //kazkas panasaus kaip dictionary C# (stringas yra raktas, o integeris - reiksme)
        mapas.put("penki", 5);
        mapas.put("sesi", 6);
        out(mapas.values()); //isveda reiksmes
        Integer valueSeseto = mapas.get("sesi");
        Integer value = mapas.get("penki");
        out(value);
        out(mapas.keySet());//isveda raktus
        Collection raktai = mapas.keySet();
        for(Integer reiksme : mapas.values())
        {
            //kazka daryti
        }
        mapas.entrySet();
        
        
        //-----------------EXCEPTIONAI-------------------------------------------------
        Player p1 = null;
        while(p1 == null)
        {
            try
            {
                new Player(inWord("ivesk varda:"), inWord("ivesk emaila:"));
            }
            catch(Exception e)
            {
                out(e.getMessage());
            }
        }
        
        try
        {
            Person per1 = new Person("", "");
        }
        catch(BadDataInputException | OffensiveDataInputException e)
        {
            out(e.getMessage());
        }
        catch(Exception e)
        {
           out(e.getMessage()); 
        }
    }//-----end of main
    
    private static Set<String> createSet(String... strings)
    {
        Set<String> result = new HashSet();
        if(strings != null)
        {
            Collections.addAll(result, strings);
        }
        return result;
    }
    
    private static List<String> createList(String... strings)
    {
        List<String> result = new ArrayList();
        if(strings != null)
        {
            Collections.addAll(result, strings);
        }
        return result;
    }
    
    private static void collectionOut(Collection col)
    {
        if(col != null)
        {
            for(Object item : col)
            {
                out(item);
            }
        }
    }
    
    
}
