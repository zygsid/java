/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.io.IOException;

/**
 *
 * @author User
 */
public class Person implements Named {
    
    private String name;
    private String surname;
    protected Gender gender;
    private int age;
    private String email;
    public Person(String name, String email) throws Exception, IOException
    {
        if(name == null || name.trim().length() == 0)//kuriam exceptiono apibrezima
        {
            throw new BadDataInputException("Name must be not null"); //kitos klases exceptionus reikia deklaruoti prie thro
        }
        else
        {
            this.name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
        }
        
        if(email == null || email.trim().length() == 0 || "vardenis".equals(email))//kuriam exceptiono apibrezima
        {
            throw new OffensiveDataInputException("Email must be not null");
        }
        else
        {
            this.email = email;
        }
        
    }
    public Person(String name, String email, Gender gender) throws Exception
    {
        this(name,email);
        this.gender = gender;
    }
    public Person(String name, String email, Gender gender, int age, String surname) throws Exception
    {
        this(name,email,gender);
        this.age = age;
        this.surname = surname;
    }
    
    @Override
    public String toString()
    {
        return getClass().getSimpleName() + "(name = " + getName() + " gender = " + gender.getEnLabel() + ")";
    }


    @Override
    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Gender getGender() {
        return gender;
    }

    public int getAge() {
        return age;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public int getId() {
        return age; //To change body of generated methods, choose Tools | Templates.
    }
}
