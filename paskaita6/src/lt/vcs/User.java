/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

/**
 *
 * @author User
 * @param <T> Person tipo parametras
 */
public class User<T extends Person>{
    
    private T person; //T bus kitamojo tipas
    
    public User(T person)
    {
        this.person = person;
    }

    public T getPerson() {
        return person;
    }
}
