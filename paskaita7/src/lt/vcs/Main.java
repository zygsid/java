/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import static java.nio.charset.StandardCharsets.UTF_8;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static lt.vcs.VcsUtils.*;

/**
 *
 * @author User
 */
public class Main {

    private static final String PVZ_FAILAS = "C:\\temp\\pvz.txt";
    private static final String TEMP_DIR = "C:\\temp";
    private static final String UTF_8 = "UTF-8";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        File pvzFile = new File(PVZ_FAILAS);
        BufferedReader br = null;
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(pvzFile), UTF_8));

            while ((line = br.readLine()) != null) //tikrina ar ne failo pabaiga
            {
                out(line);
            }
        } catch (Exception e) {
            out(e.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            }
        }

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(pvzFile), UTF_8));
        } catch (Exception e) {
            out(e.getMessage());
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            }
        }
        File file = newFile("C:\\temp", "pvzzzz");
    }

    private static File newFile(String dir, String fileName) {
        File result = null;
        Path path = Paths.get(dir);
        String name = path + File.separator + fileName;
        File file = new File(name);
        if (dir == null || fileName == null || !Files.isDirectory(path)) {
            out("Dir is null/doesn't exist or fileName is null!");
            return result;
        }
        if (Files.isDirectory(path)) {
            File f = new File(dir);
            if (f.exists()) {
                try {
                    file.createNewFile();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            } else {
                f.mkdirs();
                try {
                    file.createNewFile();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            }
            result = file;
        }
        return result;
    }
}
