/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import static lt.vcs.VcsUtils.*;

/**
 *
 * @author User
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // url points to jdbc protocol : mysql subprotocol; localhost is the address
        // of the server where we installed our DBMS (i.e. on local machine) and
        // 3306 is the port on which we need to contact our DBMS
        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "vcs_17_04";
        String userName = "root";
        String password = "";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url + dbName, userName, password);
            out("Prisijungem prie DB sekmingai!");
            out(conn.getMetaData().getURL());
            Statement s = conn.createStatement();
            s.executeUpdate("insert into person values(" + (getLastId(s,"person")+1) + ", 'As', 'Mano', 2, 77, 'as@mano.lt');");
            ResultSet rs = s.executeQuery("select * from person ;");
            while (rs.next()) {
                
                String name = rs.getString("name");
                String email = rs.getString("email");
                int id = rs.getInt("id");
                int gender = rs.getInt("gender");
                out("id:" + id + "\t\tname:" + name + "\t\temail:" + email + "\t\tgender:" + gender);// \t - tabuliacija (lygiavimas)
                out("name: " + rs.getMetaData().getColumnName(2) + " type: " + rs.getMetaData().getColumnTypeName(2));
            }
        } catch (Exception e) {
            out("Nepavyko prisijungti prie DB: " + e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            }

        }

    }
    
        private static int getLastId(Statement s, String table)
        {
            Integer result = null;
            try{
            ResultSet rs = s.executeQuery("select id from " + table + " order by id desc limit 1;");
            if(rs.next())
            {
                result = rs.getInt(1);
            }
            }
            catch(Exception e)
            {
                out(e.getMessage());
            }
            
            return result;
        }

}
