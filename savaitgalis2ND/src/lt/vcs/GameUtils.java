/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import static lt.vcs.VcsUtils.*;

/**
 *
 * @author RBS
 */
public class GameUtils {

    public static int getFirstDeposit(String zaidejas) //ivedame, kuris zaidejas depozita darys
    {
        out(zaidejas + " zaidejas iveda statymo suma:");
        int deposit = 0;
        while (true) {
            deposit = inInt();
            if (deposit <= Player.getStartMoney() && deposit != 0) {
                break;
            } else {
                out("Turite ivesti statymo suma iki " + Player.getStartMoney() + "!");
            }
        }
        return deposit;
    }

    public static int getSecondDeposit(String zaidejas, int p1Deposit) {
        int deposit = 0;
        out(zaidejas + " zaidejas iveda savo statyma (ne maziau uz " + p1Deposit + "):");
        while (true) {
            deposit = inInt();
            if (deposit < p1Deposit || deposit > Player.getStartMoney()) {
                out("Turite ivesti atsakomaja suma nuo " + p1Deposit + " iki " + Player.getStartMoney() + "!");
            } else {
                break;
            }
        }
        return deposit;
    }
    
    public static int getRaise(int remainingMoney)
    {
        int deposit = 0;
        while(true)
        {
            deposit = inInt();
            if(deposit > remainingMoney || deposit <= 0)
            {
                out("Turite ivesti statymo kelimo suma nuo 0 iki " + remainingMoney + "!");
            }
            else break;
        }
        return deposit;
    }

    public static int getWinner(Player p1, Player p2)
    {
        if(p1.getDiceHandScore() > p2.getDiceHandScore())
        {
            out("Laimejo " + p1.getName() + "! Jo kauliuku akuciu suma yra " + p1.getDiceHandScore());
            return 1;
        }
        else if(p1.getDiceHandScore() < p2.getDiceHandScore())
        {
            out("Laimejo " + p2.getName() + "! Jo kauliuku akuciu suma yra " + p2.getDiceHandScore());
            return 2;
        }
        else 
        {
            out("Lygiosios! Pinigai buvo grazinti abiem zaidejams!");
            return 0;
        }
        
    }
}
