/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import static lt.vcs.VcsUtils.*;
import static lt.vcs.GameUtils.*;

/**
 *
 * @author RBS
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        out("Iveskite pirmo zaidejo varda:");
        Player p1 = new Player(inLine());
        out("Iveskite antro zaidejo varda:");
        Player p2 = new Player(inLine());
        out("Abu zaidejai prades zaidima su " + Player.getStartMoney() + " pinigu");
        int p1Deposit = 0;
        int p2Deposit = 0;
        int p1RemainingMoney = Player.getStartMoney();
        int p2RemainingMoney = Player.getStartMoney();

        while (true) {
            p1Deposit = getFirstDeposit("Pirmas");
            p2Deposit = getSecondDeposit("Antras", p1Deposit);
            p1RemainingMoney -= p1Deposit;
            p2RemainingMoney -= p2Deposit;
            if (p2Deposit == p1Deposit) {
                int checkinam = getWinner(p1, p2); // checkinam kas laimejo (0 - lygiosios, 1 - p1, 2 - p2)
                if (checkinam == 0) {
                    out("Lygiosios! Pinigai keliauja atgal pas zaidejus!");
                    p1RemainingMoney += p1Deposit;
                    p2RemainingMoney += p2Deposit;
                    p1Deposit = 0;
                    p2Deposit = 0;
                    out("Norint baigti zaidima iveskite 1");
                    int checkIfContinuesToPlay = inInt();
                    if (checkIfContinuesToPlay == 1) {
                        break;
                    }

                } else if (checkinam == 1) {
                    out("Laimejo " + p1.getName() + "!");
                    p1RemainingMoney += p1Deposit + p2Deposit;
                    p1Deposit = 0;
                    p2Deposit = 0;
                    out("Norint baigti zaidima iveskite 1");
                    int checkIfContinuesToPlay = inInt();
                    if (checkIfContinuesToPlay == 1) {
                        break;
                    }
                } else if (checkinam == 2) {
                    out("Laimejo " + p2.getName() + "!");
                    p2RemainingMoney += p1Deposit + p2Deposit;
                    p1Deposit = 0;
                    p2Deposit = 0;
                    out("Norint baigti zaidima iveskite 1");
                    int checkIfContinuesToPlay = inInt();
                    if (checkIfContinuesToPlay == 1) {
                        break;
                    }
                }

            } else {
                while (true) {
                    if(p1RemainingMoney == 0)
                    {
                        out(p1.getName() + " nebeturi pinigu! Zaidimas baigtas!");
                        break;
                    }
                    if(p2RemainingMoney == 0)
                    {
                        out(p2.getName() + " nebeturi pinigu! Zaidimas baigtas!");
                        break;
                    }
                    out("Pirmas zaidejas turi arba prilyginti suma iki " + p2Deposit + ", arba ja pakelti (likutis pinigu yra " + p1RemainingMoney + ")");
                    p1Deposit += getRaise(p1RemainingMoney);
                    p1RemainingMoney = Player.getStartMoney() - p1Deposit;
                    out("Antras zaidejas turi arba prilyginti suma iki" + p1Deposit + ", arba ja pakelti (likutis pinigu yra " + p2RemainingMoney + "!");
                    p2Deposit += getRaise(p2RemainingMoney);
                    p2RemainingMoney = Player.getStartMoney() - p2Deposit;
                    if (p1Deposit == p2Deposit) {
                        int checkinam = getWinner(p1, p2); // checkinam kas laimejo (0 - lygiosios, 1 - p1, 2 - p2)
                        if (checkinam == 0) {
                            out("Lygiosios! Pinigai keliauja atgal pas zaidejus!");
                            p1RemainingMoney += p1Deposit;
                            p2RemainingMoney += p2Deposit;
                            p1Deposit = 0;
                            p2Deposit = 0;
                            out("Norint baigti zaidima iveskite 1");
                            int checkIfContinuesToPlay = inInt();
                            if (checkIfContinuesToPlay == 1) {
                                break;
                            }

                        } else if (checkinam == 1) {
                            out("Laimejo " + p1.getName() + "!");
                            p1RemainingMoney += p1Deposit + p2Deposit;
                            p1Deposit = 0;
                            p2Deposit = 0;
                            out("Norint baigti zaidima iveskite 1");
                            int checkIfContinuesToPlay = inInt();
                            if (checkIfContinuesToPlay == 1) {
                                break;
                            }
                        } else if (checkinam == 2) {
                            out("Laimejo " + p2.getName() + "!");
                            p2RemainingMoney += p1Deposit + p2Deposit;
                            p1Deposit = 0;
                            p2Deposit = 0;
                            out("Norint baigti zaidima iveskite 1");
                            int checkIfContinuesToPlay = inInt();
                            if (checkIfContinuesToPlay == 1) {
                                break;
                            }
                        }
                    }
                }
            }
        }

    }

}
