/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;
import static lt.vcs.VcsUtils.*;

/**
 *
 * @author RBS
 */
public class Player {
    
    private static int startMoney;
    private String name;
    private int dice;
    private int[] diceHand = new int[2];
    
    public Player(String name)
    {
        this.startMoney = 250;
        this.name = name;
        this.dice = random(1,6);
        for(int i=0;i<2;i++)
        {
        this.diceHand[i] = random(1,6);
        }
    }

    public static int getStartMoney() {
        return startMoney;
    }

    public String getName() {
        return name;
    }

    public int getDice() {
        return dice;
    }

    public int[] getDiceHand() {
        return diceHand;
    }
    public int getDiceHandScore()
    {
        int score = 0;
        for(int i=0;i<2;i++)
        {
            score += this.diceHand[i];
        }
        return score;
    }
}
